<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package multilogin_theme
 */

get_header();
?>

	<div id="primary" class="content-area">
<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
<div id="single-wrap">
  <div class="back-img back-img col-5 offset-1 pt-6 pr-0">
    <img class="why-back" src="<?php echo $url ?>">
  </div>
  <div class="container pb-6">
    <div class="row pt-col">
      <div class="col-6 why-join_text">
        <h1 class="large-f pb-2"><?php the_title(); ?> [<?php the_field('job_location'); ?>]</h1>
        <div class="single-content">
            <div class="single-top">
                <div class="single-excerpt pb-5 pt-4"><?php the_excerpt(); ?></div>
                <a href="<?php the_field('job_link'); ?>" class="btn btn-gradient btn-glow" <?php if( get_field('recruit_form') ): ?><?php else: ?>target="_blank"<?php endif; ?>>Apply now!</a>
            </div><!-- top -->
            <div class="single-bottom pt-7">
                <div class="single-bottom_wrap">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                <?php endwhile; endif; ?>
                </div>
            </div>
        </div><!-- content -->
      </div><!-- text -->
    </div><!-- upper row -->
      <div class="icons-col pt-3 row">
        <div class="icons-title pt-6 pb-5 col-6">
            <h2 class="large-f"><?php the_field('b-title'); ?></h2>
            <p class="regular-f pt-3"><?php the_field('b-subtitle'); ?></p>
        </div>
        <div class="col-12 d-flex mx-auto">
         <?php  if( have_rows('b-icons') ):
            while ( have_rows('b-icons') ) : the_row(); ?>
          <div class="col">
              <img src="<?php the_sub_field('b-icon'); ?>" alt="Multilogin-icon">
              <div class="icon-col_text col-10 mx-auto pt-5">
                <h3><?php the_sub_field('b-text'); ?></h3>
              </div>
          </div><!-- col -->
        <?php endwhile; endif; ?>
        </div>
  </div><!-- container -->
</div><!-- #single-wrap  -->
<div id="interested">
    <div class="container pb-8">
        <div class="row">
            <div class="col-6 mx-auto pt-6 pb-6">

            <div class="ceo-box">
              <div class="row">
                <div class="col-4 text-center">
                  <div class="ceo-img mx-auto mb-3">
                    <img src="<?php the_field('ceo-img'); ?>">
                  </div>
                  <h3 class="p-0 m-0"><?php the_field('ceo-name'); ?></h3>
                  <span class="ceo-position"><?php the_field('ceo-position'); ?></span>
                </div>
                <div class="col-8 ceo-message pt-2 d-flex align-items-center">
                  <p><span class="quote-mark top-one">“</span><?php the_field('ceo-text'); ?><span class="quote-mark bottom-one">“</span></p>
                </div>
              </div>
            </div><!-- ceo-box -->

            </div><!-- col -->
        </div><!-- quote-row -->
        <div class="fly-high row">
            <div class="col-4 offset-1">
                <?php the_field('surely'); ?>
            </div>
            <div class="col-6 offset-1 apply-text">
                <h2><?php the_field('apply-title'); ?></h2>
                <div class="d-flex wrap-contacts">
                    <div class="your-contacts">
                        <h3><?php the_field('apply-title-2'); ?></h3>
                    </div>
                    <div class="apply-now_btn">
                        <a href="<?php the_field('job_link'); ?>" class="btn white-btn" <?php if( get_field('recruit_form') ): ?><?php else: ?>target="_blank"<?php endif; ?>>Apply now!</a>
                    </div>
                </div><!-- flex -->
                <div class="additional-info">
                    <p><?php the_field('additional'); ?></p>
                </div>
            </div>
        </div>
    </div>
</div><!-- #interested -->
<?php if( get_field('recruit_form') ): ?>
<div id="apply-now">
        <div class="container pb-8">
        <div class="row">
            <div class="col-6 mx-auto pt-6 pb-6">
              <div class="apply-title text-center">
                <h2>Apply now</h2>
              </div>
              <div class="form-wrap">
                <?php the_field('recruit_form'); ?>
              </div>  
            </div><!-- col 6 -->
        </div><!-- row -->
        </div><!-- container -->
</div><!-- #apply now -->
<?php endif; ?>

	</div><!-- #primary -->

<?php

get_footer();
