<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package multilogin_theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
				<div class="container">
		<div class="row d-flex">
		<div class="logo col-2 ml-0">
			<a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/logo.svg"></a>
		</div><!-- logo -->
		<div class="nav-col col d-flex">

		<nav id="site-navigation" class="main-navigation">
			
			<div class="desktop-menu">
			<?php if ( is_single() ) {
				wp_nav_menu( array(
				'menu' => 'alternative',
				'menu_id'        => 'primary-menu'
				) );
					} else {
         		wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu'
				) );
                    } ?>
			</div>

		</nav><!-- #site-navigation -->

			</Div>
		</div><!-- container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
