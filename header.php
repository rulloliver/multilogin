<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package multilogin_theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css"> -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<div class="container">
		<div class="row d-flex">
		<div class="logo col-2 ml-0">
			<a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/logo.svg"></a>
		</div><!-- logo -->
		<div class="nav-col col d-flex">

		<nav id="site-navigation" class="main-navigation">
			
			<div class="mobile-menu">
					<div class="button_container" id="toggle">
							<span class="top"></span>
							<span class="middle"></span>
							<span class="bottom"></span>
					</div>

					<div class="overlay" id="overlay">
			<?php if ( is_single() ) {
				wp_nav_menu( array(
				'menu' => 'alternative',
				'menu_id'        => 'primary-menu'
				) );
					} else {
         		wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu'
				) );
                    } ?>
					</div>
				</div><!-- mobile -->
			<div class="desktop-menu">
			<?php if ( is_single() ) {
				wp_nav_menu( array(
				'menu' => 'alternative',
				'menu_id'        => 'primary-menu'
				) );
					} else {
         		wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu'
				) );
                    } ?>
			</div>

		</nav><!-- #site-navigation -->

			</Div>
		</div><!-- container -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
