<?php get_header(); ?>

	<div id="primary" class="content-area">
    <div id="intro">
      <img class="intro-background" src="./wp-content/themes/multilogin_theme/assets/bg.jpg" alt="multilogin-background"><!-- intro back -->
      <div class="intro-content container">
        <div class="row center-block d-flex">

        <div class="intro-text offset-1 col-5 pr-0" data-aos="fade-in">
          <?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; endif; ?>
          <div class="cta d-flex align-items-center">
            <a href="<?php the_field('cta-1-link'); ?>" class="btn"><?php the_field('cta-1'); ?></a>
            <a href="<?php the_field('cta-2-link'); ?>" class="link-btn light-border"><?php the_field('cta-2'); ?></a>
          </div><!-- cta -->
        </div><!-- text -->

        <div class="intro-illustration col-6 pr-0" data-aos="fade-up">
          <img class="home-illustration" src="./wp-content/themes/multilogin_theme/assets/header.svg" alt="multilogin-illustration">
        </div><!-- illustration -->

        </div><!-- row -->

      </div><!-- content -->
    </div><!-- #intro -->

    <div id="do-join" class="container pt-col">
        <div class="row text-blocks" data-aos="fade-up">
          <div class="col-5">
            <h2><?php the_field('why-1'); ?></h2>
          </div>
          <div class="col-3">
            <p><?php the_field('why-2'); ?></p>
          </div>
          <div class="col-3 offset-1 pl-0">
            <p><?php the_field('why-3'); ?></p>
          </div><!-- col-3 -->
        </div><!-- # row -->
        <div class="row icons-col pt-6 pb-6">
         <?php  if( have_rows('icon-set') ):
            while ( have_rows('icon-set') ) : the_row(); ?>
          <div class="col" data-aos="fade-up">
              <img src="<?php the_sub_field('icon-set_icon'); ?>" alt="Multilogin-icon">
              <div class="icon-col_text col-10 mx-auto pt-5">
                <h3><?php the_sub_field('icon-title'); ?></h3>
                <p class="icon-p pt-2"><?php the_sub_field('icon-txt'); ?></p>
              </div>
          </div><!-- col -->
        <?php endwhile; endif; ?>
        </div>

        </div> <!-- why join -->
    </div><!-- why join -->
    <div class="story-wrap" style="background:url('./wp-content/themes/multilogin_theme/assets/full-bg.png');">
    <div id="story" class="container">
      <div class="row">
        <div class="col-4 pt-6 pb-8 the-story" data-aos="fade-up">
          <?php the_field('story'); ?>
        </div>
      </div>
    </div><!-- #story -->
      <div class="col-7 story-img pr-0 offset-1" style="background:url('<?php the_field('story-img'); ?>');" data-aos="fade-up">
      </div>
    </div><!-- story wrap -->
  
    <div id="really">
      <div class="container">
        <div class="row pt-col">
          <div class="tabs-title col-12" data-aos="fade-up">
            <h2 class="large-f pb-4"><?php the_field('tabs-title'); ?></h2>
          </div>

      <div class="col-12 mb-8" data-aos="fade-up">

				<nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Developers</a>
						<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Customer Support</a>
					</div>
				</nav>

				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					<div class="tab-pane fade show active row" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
						<div class="tab-pane_inner col-10 mx-auto pl-0 pr-0">
             <?php  if( have_rows('tabs-1') ): while( have_rows('tabs-1') ) : the_row();  ?>
              <div class="tab-quote d-flex row pt-6 pb-5">
                <div class="col-2 text-center">
                  <div class="ceo-img mx-auto mb-3">
                    <img src="<?php the_sub_field('quote_img'); ?>">
                  </div>
                  <h3 class="p-0 m-0"><?php the_sub_field('quote-name'); ?></h3>
                  <span class="ceo-position"><?php the_sub_field('quote-position'); ?></span>
                </div>
                <div class="col-7 pl-5">
                  <div class="the_quote"><span class="quote-mark top-one">“</span><p><?php the_sub_field('the-quote'); ?></p><span class="quote-mark bottom-one">“</span></div>
                </div>
                <div class="col-2 offset-1 mx-auto">
                  <a href="<?php the_sub_field('cta-3-link'); ?>" class="btn btn-gradient"><?php the_sub_field('cta-3'); ?></a>
                </div>

              </div><!-- quote -->
              <div class="tab-cols row pb-5 pt-2">
                <div class="col-4">
                  <?php the_sub_field('q_col-1'); ?>
                </div>
                <div class="col-4">
                  <?php the_sub_field('q_col-2'); ?>
                </div>
                <div class="col-4">
                  <?php the_sub_field('q_col-3'); ?>
                </div>
              </div><!-- tab cols -->
            <?php endwhile; endif; ?>
            </div><!-- tab inner -->
					</div><!-- tab pane -->
					<div class="tab-pane fade row" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
						<div class="tab-pane_inner col-10 mx-auto pl-0 pr-0">
                         <?php  if( have_rows('tabs-2') ): while( have_rows('tabs-2') ) : the_row();  ?>
              <div class="tab-quote d-flex row pt-6 pb-5">
                <div class="col-2 text-center">
                  <div class="ceo-img mx-auto mb-3">
                    <img src="<?php the_sub_field('quote_img'); ?>">
                  </div>
                  <h3 class="p-0 m-0"><?php the_sub_field('quote-name'); ?></h3>
                  <span class="ceo-position"><?php the_sub_field('quote-position'); ?></span>
                </div>
                <div class="col-7 pl-5">
                  <div class="the_quote"><span class="quote-mark top-one">“</span><p><?php the_sub_field('the-quote'); ?></p><span class="quote-mark bottom-one">“</span></div>
                </div>
                <div class="col-2 offset-1 mx-auto">
                  <a href="<?php the_sub_field('cta-4-link'); ?>" class="btn btn-gradient"><?php the_sub_field('cta-4'); ?></a>
                </div>

              </div><!-- quote -->
              <div class="tab-cols row pb-5 pt-2">
                <div class="col-4">
                  <?php the_sub_field('q_col-1'); ?>
                </div>
                <div class="col-4">
                  <?php the_sub_field('q_col-2'); ?>
                </div>
                <div class="col-4">
                  <?php the_sub_field('q_col-3'); ?>
                </div>
              </div><!-- tab cols -->
            <?php endwhile; endif; ?>

            </div><!-- tab inner -->
					</div>
				</div>
      </div><!-- col -->

        </div><!-- row -->
      </div><!-- container -->
    </div><!-- tabs -->

    <div class="video-wrap" style="background:url('<?php the_field('full-bg'); ?>');">
    <div id="ceo-message">
      <div class="container">
        <div class="row justify-content-center d-flex">
          <div class="col-6 pt-10 pb-10 offset-2">

            <div class="ceo-box" data-aos="fade-up">
              <div class="row">
                <div class="col-4 text-center">
                  <div class="ceo-img mx-auto mb-3">
                    <img src="<?php the_field('ceo-img'); ?>">
                  </div>
                  <h3 class="p-0 m-0"><?php the_field('ceo-name'); ?></h3>
                  <span class="ceo-position"><?php the_field('ceo-position'); ?></span>
                </div>
                <div class="col-8 ceo-message pt-2">
                  <div class="the_quote"><p><span class="quote-mark top-one">“</span><?php the_field('ceo-text'); ?><span class="quote-mark bottom-one">“</span></p></div>
                </div>
              </div>
            </div>

          </div>
          <div class="col-2 d-flex align-items-center">
              <div class="video-wrap mx-auto" data-aos="fade-up">
     <!-- Button trigger modal -->
<button type="button" class="btn video-btn btn-gradient" data-toggle="modal" data-src="<?php the_field('video-src'); ?>" data-target="#modal">
 <span class="play-icon" style="background:url('./wp-content/themes/multilogin_theme/assets/play.png"></span> Play video
</button>

              </div>
          </div>
        </div>
      </div>
    </div>
    </div><!-- full bg -->
  </div><!-- #primary -->

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button type="button" class="close btn btn-gradient smaller-btn" data-dismiss="modal" aria-label="Close">
          Close
        </button> 
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">       
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
        </div>
      </div><!-- body -->
    </div><!-- content -->
  </div><!-- dialog -->
</div> <!-- #modal -->
<!-- end modal -->

<div id="why-join">
  <div class="back-img back-img col-5 offset-1 pt-10 pr-0">
    <img class="why-back" src="<?php the_field('w-back'); ?>" data-aos="fade-up">
  </div>
  <div class="container pb-10">
    <div class="row pt-col">
      <div class="col-6 why-join_text">
        <h2 class="large-f pb-2" data-aos="fade-up"><?php the_field('w-title'); ?></h2>
        <div class="why-blocks">
        <?php  if( have_rows('why') ):
            while ( have_rows('why') ) : the_row(); ?>

          <div class="single-why row pt-5" data-aos="fade-up">
            <div class="col-2">
              <img class="why-icon" src="<?php the_sub_field('w-icon'); ?>">
            </div>
            <div class="col-10 why-text">
              <?php the_sub_field('w-text'); ?>
            </div><!-- col - 9 -->
          </div><!-- single-why -->
        <?php endwhile; endif; ?>
        
          <div class="why-cta text-center pt-5" data-aos="fade-up">
            <a href="<?php the_field('cta-5-link'); ?>" class="btn btn-gradient"><?php the_field('cta-5'); ?></a>
          </div>

        </div>
      </div><!-- text -->
    </div>
  </div>
</div><!-- #why join -->

<div id="benefits">
  <div class="container">
    <div class="row">
      <div class="b-title pt-6 col-5" data-aos="fade-up">
        <h2 class="large-f"><?php the_field('b-title'); ?></h2>
        <p class="regular-f pt-4"><?php the_field('b-subtitle'); ?></p>
      </div>
      <div class="row icons-col pt-5 pb-5">
        <div class="col-12 row mx-auto">
         <?php  if( have_rows('b-icons') ):
            while ( have_rows('b-icons') ) : the_row(); ?>
          <div class="col" data-aos="fade-up">
              <img src="<?php the_sub_field('b-icon'); ?>" alt="Multilogin-icon">
              <div class="icon-col_text col-10 mx-auto pt-5">
                <h3><?php the_sub_field('b-text'); ?></h3>
              </div>
          </div><!-- col -->
        <?php endwhile; endif; ?>
      </div>
    </div>
    <div class="bottom-row col-8 mx-auto pt-3 pb-8 text-center" data-aos="fade-up">
      <p class="regular-f"><?php the_field('b-bottom');?></p>
    </div>
  </div>
</div>
</div><!-- #benefits -->

<div id="video-slider" class="bg pb-9">
  <div class="container">
    <div class="row">
      <div class="col">
      <div class="v-title pt-col" data-aos="fade-up">
        <h2 class="text-white large-f pb-5"><?php the_field('v-title'); ?></h2>
      </div><!-- title -->
      <div class="slider-wrap">
      <div class="slider slider-video">

         <?php  if( have_rows('videos') ):
            while ( have_rows('videos') ) : the_row(); ?>
        <div class="single-video">
          <div class="inner-video">
              <div class="video-thumbnail" style="background:url('<?php the_sub_field('thumb'); ?>);"></div>
              <button class="video-btn" data-toggle="modal" data-src="<?php the_sub_field('video_src'); ?>" data-target="#modal"><img src="./wp-content/themes/multilogin_theme/assets/play2.svg"></button>
          </div><!-- inner -->
        </div><!-- single -->
        <?php endwhile; endif; ?>
      </div>

        <div class="slick-arrow_wrap">
          <div class="prev-slick video-s"><img src="./wp-content/themes/multilogin_theme/assets/arrow-l.png"></div>
          <div class="next-slick video-s"><img src="./wp-content/themes/multilogin_theme/assets/arrow-r.png"></div>
        </div><!-- arrows -->
      </div><!-- slider-wrap -->
    </div>
    </div>
    </div>
  </div><!-- video slider -->

  <div id="jobs">
    <div class="container">
      <div class="row">
      <div class="col-7">
      <div class="b-title jobs-title pt-6" data-aos="fade-up">
        <h2 class="large-f"><?php the_field('jobs-title'); ?></h2>
      </div>
      <div class="jobs-wrap pb-8">
           <?php $post_type = 'jobs';
                  $taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
                  foreach( $taxonomies as $taxonomy ) : $terms = get_terms( $taxonomy ); foreach( $terms as $term ) : ?>

          <h2 class="mid-heading pt-5 pb-2"><?php echo $term->name; ?></h2>

          <?php $args = array(
            'post_type' => $post_type,
            'posts_per_page' => -1,  //show all posts
            'tax_query' => array(
                array(
                    'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => $term->slug,
            )));
          $posts = new WP_Query($args); if( $posts->have_posts() ): while( $posts->have_posts() ) : $posts->the_post(); ?>
            <div class="single-position d-flex" data-aos="fade-up" data-aos-offset="-100">
              <div class="position-title">
                <a href="<?php echo get_permalink(); ?>" title="Read more about <?php echo get_the_title(); ?>"><?php  echo get_the_title(); ?></a>
              </div>
              <div class="position-cat">
                  <?php echo $term->name; ?>
              </div>
              <div class="position-link">
                <a href="<?php echo get_permalink(); ?>" class="btn btn-gradient smaller-btn" title="Read more about <?php echo get_the_title(); ?>">View job</a>
              </div>
            </div>

          <?php wp_reset_postdata(); ?>
          <?php endwhile; endif; ?><?php endforeach; endforeach; ?>
    </div>
    </div><!-- col 8 -->
    <div class="col-4 offset-1">
      <div class="jobs-exceprt b-title pt-6" data-aos="fade-up">
        <?php the_field('jobs-excerpt'); ?>
      </div>
    </div><!-- col -4 -->
  </div>
  </div>
</div><!-- #jobs -->

  <div id="img-slider" class="bg pb-9">
  <div class="container">
    <div class="row">
      <div class="col">
      <div class="v-title pt-col">
        <h2 class="text-white large-f pb-5"><?php the_field('img-title'); ?></h2>
      </div><!-- title -->
      <div class="slider-wrap">
      <div class="slider slider-img">

         <?php  if( have_rows('images_slider') ):
            while ( have_rows('images_slider') ) : the_row(); ?>
        <div class="single-video">
          <div class="inner-video">
              <div class="video-thumbnail" style="background:url('<?php the_sub_field('img'); ?>);"></div>
              <button class="video-btn" data-toggle="modal" data-src="<?php the_sub_field('img'); ?>" data-target="#img-modal"></button>
          </div><!-- inner -->
        </div><!-- single -->
        <?php endwhile; endif; ?>
      </div>

        <div class="slick-arrow_wrap">
          <div class="prev-slick img-s"><img src="./wp-content/themes/multilogin_theme/assets/arrow-l.png"></div>
          <div class="next-slick img-s"><img src="./wp-content/themes/multilogin_theme/assets/arrow-r.png"></div>
        </div><!-- arrows -->
        </div><!-- slider-wrap -->
  </div>
    </div>
  </div><!-- video slider -->


</div>


<?php
get_footer();