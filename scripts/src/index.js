import jQuery from 'jquery'
import Router from './util/Router'

// Naming in camelCase is important for the router to match the route
import common from './routes/common'

// Load styles
import '../../_sass/main.scss'
import '../../_sass/_normalize.scss'

// Import js
import '../../javascripts/scripts.js'
import '../../javascripts/bootstrap-4-latest.min.js'
import '../../javascripts/slick.min.js'
import '../../javascripts/jquery.nav.js'
import AOS from 'aos'
import 'aos/dist/aos.css' // You can also use <link> for styles
// ..
AOS.init()

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
  /** All pages */
  common,
  /** About Us page, note the change from about-us to aboutUs. */
})

/** Load Events */
jQuery(document).ready(() => routes.loadEvents())
