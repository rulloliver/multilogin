import $ from 'jquery'

export default {
  init () {
    // JavaScript to be fired on all pages
    $('.slider.slider-video').slick({
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      dots: false,
      slidesToScroll: 1,
      prevArrow: $('.prev-slick.video-s'),
      nextArrow: $('.next-slick.video-s'),
      responsive: [
        {
          breakpoint: 1124,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 650,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    })
    // header class for mobile 
    var elementPosition = $('#masthead').offset();
    $(window).scroll(function(){
        if($(window).scrollTop() > 100 + elementPosition.top){
              $('#masthead').addClass("lower");
        } else {
            $('#masthead').removeClass("lower");
        }  
    // IMG SLIDER
    $('.slider.slider-img').slick({
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      dots: false,
      slidesToScroll: 1,
      prevArrow: $('.prev-slick.img-s'),
      nextArrow: $('.next-slick.img-s'),
      responsive: [
        {
          breakpoint: 1124,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 650,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    })

    // MENU JQUERY

    $('#toggle').click(function () {
      $(this).toggleClass('active')
      $('#overlay').toggleClass('open')
    })

    $('.overlay li').click(function () {
      $('#overlay').removeClass('open')
      $('#toggle').removeClass('active')
    })

    // SMOOTH SCROLL BUTTONS
    $(document).on('click', 'a[href^="#"]', function (e) {
      e.preventDefault()
      var target = this.hash
      var $target = $(target)
      $('html, body').stop().animate({
        scrollTop: $target.offset().top - 0
      }, 1000)
    })

    // SMOOTH SCROLL MENU
    $('#primary-menu').onePageNav({
      currentClass: 'current',
      changeHash: false,
      scrollSpeed: 750,
      scrollThreshold: 0.5,
      filter: '',
      easing: 'swing',
      begin: function () {
        // I get fired when the animation is starting
      },
      end: function () {
        // I get fired when the animation is ending
      },
      scrollChange: function ($currentListItem) {
        // I get fired when you enter a section and I pass the list item of the section
      }
    })
    // Gets the video src from the data-src on each button
    var $videoSrc
    $('.video-btn').click(function () {
      $videoSrc = $(this).data('src')
      console.log($videoSrc)
    })
    // when the modal is opened autoplay it
    $('#modal').on('shown.bs.modal', function () {
      // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
      $('#video').attr('src', $videoSrc + '?autoplay=1&amp;modestbranding=1&amp;showinfo=0')
      console.log('shown')
    })
    // stop playing the youtube video when I close the modal
    $('#modal').on('hide.bs.modal', function (e) {
    // a poor man's stop video
      $('#video').attr('src', $videoSrc)
      console.log('hidden')
    })
  },
  finalize () {
    // JavaScript to be fired on all pages, after page specific JS is fired
  }
}
